<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alarma extends Model {

    protected $table = 'alarmas';
    protected $primaryKey = 'id';
    public $timestamps = true;
    public $incrementing = true;
    
}