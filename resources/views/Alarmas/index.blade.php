@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Formulario Anunciate 
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="container-fluid">
			    <div class="row">
			        <div class="col-md-3 col-md-offset-1">
			            <div class="box box-solid">
 
			            </div>
			        </div>
			    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
	    	@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
			@endif
          <div class="box">
			<div class="box-body">
	            <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    	<tr class="header">
							<th>ID</th>
							<th>Nombre</th>
							<th>Estado</th>
							
						</tr>
                    </thead>
                    <tbody>
						@foreach ($alarmas as $alarma)
						<tr>
							<td>{{ $alarma->id }}</td>
							<td>{{ $alarma->nombre_alarma }}</td>
							<td>{{ $alarma->estado }}</td>
							
						
						</tr>
						@endforeach 
                       
                    </tbody>
                </table>
            </div>
		  </div>
        </div>
    </div>
@endsection
